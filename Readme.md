# 3D mapping using point clouds
To Implement different systems for capturing and reconstructing 3D data in real time that gives us the respective information of external environments, this task requires some computational effort to acquire the sensor information and to fuse the data in a Point Cloud.  The main objective of this research project is to develop an efficient and low-cost system, capable of recording a sequence of 3D point clouds captured by the Kinect to generate a 3D geometric color model of an outdoor environment.

The present repository shows the results obtained by using algorithms applied to three-dimensional reconstruction for the processing of point clouds from 3D scans of different improvised scenes or outdoor environments, through a series of modular libraries from PCL and Open3D overlapping tools.



## Repository Folders
This repository contents:
```
/your_root        - path
|--data                 /Folder where you can find the project data
    |--Example 1        /3D reconstruction Hall "Casona" 1
    |--Exmaple 2        /3D reconstruction Hall "Casona" 2
    |--Exmaple 3        /3D reconstruction Hall "Casona" 3
    |--Example 4        /full 3D mapping "Casona" 
    |--Example data     /Laboratory data at the University of Ibagué
    |--Example data     /Point Cloud data Cocoa crop

|--Readme   /instructions for use the mapping 3D software 

|--src      /scripts for the Activity Registration 3D
    |--Kinect v1    /All c++ scripts developed for kinect v1 
        |--Capture Point Cloud      /Algorithm to get 3D point cloud
        |--Keypoints                /Scripts to keypoint estimation
            |--HARRIS 3D            /Script to get Harris 3D keypoint
            |--SIFT                 /Script to get SIFT keypoint
            |--AGAST                /Script to get AGAST keypoint
            |--ISS 3D               /Script to get ISS 3D keypoint
        |--Parwise incremental Registration     /C++ script to registration 3D 
    |--Kinect v2        /All python scripts developed for kinect v2
        |--Multiway Registration    /Python script to registration 3D 
```
## Hardware elements
- Kinect v1/v2
- PC
- tripod
- 12v source
- DC / AC inverter
- Core i5 processor onwards
## Software requirements
- Ubuntu 16.04
- Point Cloud Library (PCL) 1.7.x – 1.8.x
- Open 3D
- ROS Kinect/Melodic 
- Install libfreenect2 version 0.2.0
- Install iai_kinect2

### How to Install PCL
1. Open Terminal
2. Run the following lines of code
`$ sudo add-apt-repository ppa: v-launchpad-jochen-sprickerhof-de /pcl`
`$ sudo apt-get update`
`$ sudo apt-get install libpcl-all`
3. follow the instructions in the following repository:
 [Install PCL](https://larrylisky.com/2016/11/03/point-cloud-library-on-ubuntu-16-04-lts/)
### How to Run PCL
Once PCL is installed, we proceed to visualize our environment through Kinect, creating two types of files. The first is the Cmake compiler, usually called CmakeLists.txt, there will be located the different libraries and executables that will be needed to execute the algorithm. And the second is a .CPP file, that is, with C ++ programming language.
1. Open Terminal
2. Go to the work folder
ex: `$ cd/PATH/TO/MY/GRAND/PROJECT`
3. Create build folder
`$ mkdir build`
4. Go to build
`$ cd build`
5. Building the project
`$ cmake ..`
`$ make`
6. Run the executable file
`$ ./code`
### How to Install Open3D
1. follow the instructions in the following repository:
[Install Open3D version 1](http://www.open3d.org/docs/release/getting_started.html)
[Install Open3D version 2 ](http://robotslam.blogspot.com/2018/03/open3d-modern-library-for-3d-data.html)
### Proceedings to install Microsoft Kinect v2
1. Install ROS kinetic for Ubuntu 16.04
[ROS kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu)
2. Configure your ROS environment
[ROS environment](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment)
3. Install libfreenect2 version 0.2.0
    - Check requirements:
        1. Connect Kinect and do:
        `$ lsusb -t` and `$ lspci | grep USB`
        2. Check OpenCV version 2.4.x
        [Install opencv-2.4.13](https://gist.github.com/KhaledSMQ/081a6fffca92d50ab8b634d1b5b795ae)
        Check version:
        `$ pkg-config --modversion opencv`
        3. Check Eigen:
        `$sudo apt-get install libeigen3-dev`
    - Download libfreenect2 source
        `$ git clone https://github.com/OpenKinect/libfreenect2.git`
        `$ cd ~/libfreenect2/`
    - Install build tools
    `$ sudo apt-get install build-essential cmake pkg-config`
    - Install libusb version 1.0.20
    `$sudo apt-get install libusb-1.0-0-dev`
    - Install TurboJPEG 1.4.2
    `$ sudo apt-get install libturbojpeg libjpeg-turbo8-dev`
    - Install OpenGL 3.1
        `$ sudo apt-get install libglfw3-dev`
        `$ glxinfo | grep "OpenGL version"`
    - Install OpenCL 1.1
    `$ sudo add-apt-repository ppa:floe/beignet && sudo apt-get update `
    `$ sudo apt-get install beignet beignet-dev opencl-headers`
    - Install CUDA
        `$apt-get install cuda`
        `$ sudo prime-select intel`
    - Install VAAPI
    `$ sudo apt-get install libva-dev libjpeg-dev`
    - Install OpenNI2 version 2.2.0.33
    `$ sudo apt-get install libopenni2-dev`
    - Build
    `$mkdir build && cd build`
    `$cmake .. -Dfreenect2_DIR=$HOME/freenect2/lib/cmake/freenect2`
    `$-DENABLE_CXX11=ON -DCMAKE_INSTALL_PREFIX=$HOME/freenect2`
    `$make`
    `$make install`
    - Set up udev rules for device access
    `$sudo cp ../platform/linux/udev/90-kinect2.rules /etc/udev/rules.d/`
    Check if the idProduct of your sensor is in the file list. If not just add another line with the idProduct of your sensor. You can obtain it by running `$dmesg | grep "045e"`.
    Reconnect the sensor and you should be able to access it.
    - Run the test program
    `$./Protonect gl`to test OpenGL support
    `$./Protonect cl`to test OpenCL support
    `$./Protonect cpu`to test CPU support
    - Run OpenNI2 test
    `$ sudo apt-get install openni2-utils && sudo make install-openni2 && NiViewer2`
4. Install iai_kinect2
`$cd ~/catkin_ws/src/`
`$git clone https://github.com/code-iai/iai_kinect2.git`
`$cd iai_kinect2`
`$rosdep install -r --from-paths .`
`$cd ~/catkin_ws`
`$catkin_make -Dfreenect2_DIR=$HOME/freenect2/lib/cmake/freenect2 -DCMAKE_BUILD_TYPE="Release"`
5. Connect your sensor and run `kinect2_bridge`
- If all `Protonect`dependencies are working and showing color, depth and IR images:
`$ roslaunch kinect2_bridge kinect2_bridge.launch`
- If some dependencies are working, for example `opengl` and `cpu` but not `opencl`:
`$ roslaunch kinect2_bridge kinect2_bridge.launch depth_method:=opengl depth_device:=0 reg_method:=cpu reg_device:=0`
6. Calibrate your sensor using the `kinect2_calibration`. 
7. Add the calibration files to the `kinect2_bridge/data/<serialnumber>`folder. Further details
8. Restart `kinect2_bridge` and view the results using `rosrun kinect2_viewer kinect2_viewer kinect2 sd cloud`.
### Proceedings to save data Microsoft Kinect v2
1. Install files to save data
    - Install some drivers
`$sudo apt-get update`
`$sudo apt-get install ros-kinetic-pcl-conversions`
`$sudo apt-get install ros-kinetic-pcl-ros`
    - Install folder my_pack
`$ git clone https://github.com/HaroldMurcia/InteligenciaArtificial`
Go to path:  `InteligenciaArtificial/kinect/data_capture_cpp/`
Copy folder `my_pack`
Paste it into the path: `catkin_ws/src`
`$ cd ~/catkin_ws/`
`$ catkin_make`
`$ catkin_make install`
2. Capture and save data
    - On command prompt: terminal 1
`roscore`
    - On command prompt: terminal 2
`$cd ~/catkin_ws/`
`$catkin_make`
`$source devel/setup.bash`
(launch kinect2_bridge.. see Connect your sensor and run `kinect2_bridge`) 
`$roslaunch kinect2_bridge kinect2_bridge.launch depth_method:=opengl depth_device:=0 reg_method:=cpu reg_device:=0 publish_tf:=true`
    - On command prompt: terminal 3
`$cd ~/catkin_ws/`
`$catkin_make`
`$source devel/setup.bash`
`$ rosrun kinect2_viewer kinect2_viewer kinect2 sd cloud`
    - On command prompt: terminal 4
`$ cd ~/catkin_ws/`
`$ catkin_make`
`$ source devel/setup.bash`
`$ rosrun my_pack k2_capture PATH_TO_FILE/FILE_NAME.txt`
### Registration on Open 3D
1. Open the terminal
2. Go to tthe rute `$ cd Open3D/examples/Python/Advanced/`
3. Open the code `multiway_registrationPro.py` and change the numbers of Point clouds
4. Run the code `$ python multiway_registrationPro.py`
## Authors:
**Universidad de Ibagué - Ingeniería Electrónica Asistencia de Investigación 2020/A**
**Asitencia de Investigación 2020/A**
- Edwin Andres Casallas Lozano
- [Harold F. Murcia](www.haroldmurcia.com)
***



