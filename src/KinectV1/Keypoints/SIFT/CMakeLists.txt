cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(example_sift_keypoint_estimation)

find_package(PCL 1.6 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

set(PCL_BUILD_TYPE Release)
 
file(GLOB PCL_REG_SRC
    "src/*.h"
    "Keypoints_visualizador.cpp"
)


add_executable (example_sift_keypoint_estimation example_sift_keypoint_estimation.cpp)
target_link_libraries (example_sift_keypoint_estimation ${PCL_LIBRARIES})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-deprecated-declarations") 
